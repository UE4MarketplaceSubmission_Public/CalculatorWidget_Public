// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class UE4CalculatorWidgetTarget : TargetRules
{
	public UE4CalculatorWidgetTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "UE4CalculatorWidget" } );
	}
}
