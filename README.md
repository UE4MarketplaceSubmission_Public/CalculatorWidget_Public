# CalculatorWidget Plugin

**Video Demo:**  
https://youtu.be/LR1UWzAnu1I

**Get the CalculatorWidget Plugin from:** https://www.unrealengine.com/marketplace/en-US/product/ee623f633eac44fdb735e730e8ce0cdd

## Enable Plugin

![0](./README.res/01_Images/0.png)

## Add Editor Numeric Calculator for property panel: Can use a calculator to any numeric input box.

![1](./README.res/01_Images/1.png)
    
![2](./README.res/01_Images/2.png)
    
![3](./README.res/01_Images/3.png)

    
## Integrate the “ExprTK”(A Mathematical Expression Parsing And Evaluation Library) into UE4: Can use a blueprint function to Evaluate any constant Mathematical Expression like “2+(3-5)*10+3^5+sqrt(9)”.

![4](./README.res/01_Images/4.png)